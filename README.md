# README #

This is the multi-machine Vagrant environment for demonstration HAProxy  Layer 4 Load Balancing include three VM, HAProxy Load Balancer and two VM web-backends.
One of web-backends have installed Apache Web Server, the second one have Nginx.

Please install vagrant-hostmanager plugin

```
#!bash

vagrant plugin install vagrant-hostmanager
```

For testing use link [http://proxy.jf-vagrant.cc](http://proxy.jf-vagrant.cc)