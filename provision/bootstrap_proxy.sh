#!/bin/bash

if [ -f /vagrant/provision/bootstrap_common ]
then
    source /vagrant/provision/bootstrap_common
else
    echo /vagrant/provision/bootstrap_common was not found - exiting
    echo Update failed 1>&2
    exit 1
fi

install_haproxy() {

add-apt-repository ppa:vbernat/haproxy-1.5
apt-get update
apt-get install haproxy

}

config_haproxy() {

cp /vagrant/provision/haproxy.cfg /etc/haproxy/haproxy.cfg
service haproxy restart

}

execute 'Installing HAProxy' install_haproxy
execute 'Configuring HAProxy' config_haproxy
