#!/bin/bash

if [ -f /vagrant/provision/bootstrap_common ]
then
    source /vagrant/provision/bootstrap_common
else
    echo /vagrant/provision/bootstrap_common was not found - exiting
    echo Update failed 1>&2
    exit 1
fi

execute 'Installing nginx' apt-get install -qy nginx

finish
