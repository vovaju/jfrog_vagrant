#!/bin/bash

if [ -f /vagrant/provision/bootstrap_common ]
then
    source /vagrant/provision/bootstrap_common
else
    echo /vagrant/provision/bootstrap_common was not found - exiting
    echo Update failed 1>&2
    exit 1
fi


#execute 'Fixing vagrant "default: stdin: is not a tty" error message' fix_stdin_is_not_a_tty
#execute 'Update repository' apt-get update
#execute 'Installing python-software-properties, vim, mc and htop' apt-get install -qy --force-yes \
#        python-software-properties python-bs4 vim mc htop

execute 'Installing Apache' apt-get install -qy --force-yes apache2
